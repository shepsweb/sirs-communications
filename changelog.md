# Change Log

## 3.0.3 - 2017-05-12
* Add getDecodedAddressAttribute method to Channel to get json_decoded version of address attribute when valid json string.