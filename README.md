# Sirs-Communications #

Provides migrations, seeds, models, events, artisan commands, and jobs to support communication management.

### Installation ###
Add the following to your composer.json

```
  composer require sirs/communications
  php artisan vendor publish
  php artisan migrate
```

Make sure any models that will be communication *recipients* of messages implement the `Sirs\Communications\Contracts\Recipient` Interface and use the `Sirs\Communications\Traits\RecipientTraits` trait.

Make sure any models that will be communication *senders* of messages implement the `Sirs\Communications\Contracts\Sender` Interface and use the `Sirs\Communications\Traits\SenderTraits` trait.

No user interface is included with in this package and is the responsibility of the consumer.

*note:* Version 4.0.0 requires laravel 5.5 or greater. If you are using an older version you will need to include `Sirs\Communications\CommunicationsServiceProvider` in config/app.php.

### Configuration ###

* recipientTypes - Array of fully qualified class names for models that will be recipients
* senderTypes - Array of fully qualified class names for models that will be senders
* routeGroupSettings - Array to be applied to communication route group. Example: `['prefix'=>'api', 'middleware' => 'auth']`
* global_scopes - Global scope to be applied to CommLogs queries
* addressTypes - Ids of ChannelTypes that represent physical addresses
* channel_types - Array for defining ChannelTypes to be seeded into the database in the format `['slug' => 1]` where 1 is the id.
* statuses - Array for defining CommunicationStatuses to be seeded into the database in the format `['slug' => 1]` where 1 is the id.
* reasons - Array for defining CommunicationReasons to be seeded into the database in the format `['slug' => 1]` where 1 is the id.


### What's included ###

* Migrations for tables
    - channel_types
    - channels
    - communication_reasons
    - communication_statuses
    - communication_logs
* Seeds (including common data)
    - ChannelType
    - CommuncationReason
    - CommunicationStatus
* Models
    - Channel
    - ChannelType
    - CommLog
    - CommunicationReason
    - CommunicationStatus
* Traits
    - RecipientTraits
    - SenderTraits
* REST API CRUD Controllers for 
    - communication logs
    - channel types 
    - channels 
    - communication statuses
    - communication reasons


### Channels
A channel is a contact info item such as an email address, phone, or mailing address.  A channel has a type (typically channel types include phone, email, mailing address, and home address).  The channel's address attribute holds the actual "location" of the channel.  For example a phone channel's address attribute would have '919-123-1234'.  Mailing, Home and other physical addresses should be encoded as json (i.e `{"street1":"123 Home St","street2":null,"city":"Town","state":"NC","zip":12345}`)

### Who do I talk to? ###

* TJ Ward - jward3@email.unc.edu
* Josh Knop - jknop@unc.edu