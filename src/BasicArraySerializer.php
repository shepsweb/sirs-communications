<?php
namespace Sirs\Communications;

use League\Fractal\Serializer\ArraySerializer;

class BasicArraySerializer extends ArraySerializer
{
    public function collection($resourceKey, array $data)
    {
        return $data;
    }

    public function item($resourceKey, array $data)
    {
        return $data;
    }
}
