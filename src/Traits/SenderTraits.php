<?php
namespace Sirs\Communications\Traits;

trait SenderTraits
{

    /**
     * Display a listing of the communications associated with this sender
     *
     * @return Collection of Communication objects
     */
    public function communications()
    {
        return $this->morphMany('Sirs\Communications\Models\CommLog', 'sender');
    }
    /**
     * Display the sender base laravel route that you set up
     *
     * @return string
     */
    public function senderBaseRoute()
    {
        $class = strtolower(join('', array_slice(explode('\\', __CLASS__), -1)));
        return $class.'s';
    }
    /**
     * Display the sender controller classname
     *
     * @return string
     */
    public function senderController()
    {
        $class = join('', array_slice(explode('\\', __CLASS__), -1));
        return $class.'Controller';
    }
    /**
     * Display the sender controller show method
     *
     * @return string
     */
    public function senderControllerShow()
    {
        return $this->senderController().'@show';
    }
    /**
     * Display the sender controller show method
     *
     * @return string
     */
    public function senderControllerStore()
    {
        return $this->senderController().'@show';
    }
    /**
     * Display the sender controller create method
     *
     * @return string
     */
    public function senderControllerCreate()
    {
        return $this->senderController().'@create';
    }
    /**
     * Display the sender controller index method
     *
     * @return string
     */
    public function senderControllerIndex()
    {
        return $this->senderController().'@index';
    }
    /**
     * Display the sender controller update method
     *
     * @return string
     */
    public function senderControllerUpdate()
    {
        return $this->senderController().'@update';
    }
    /**
     * Display the sender controller edit method
     *
     * @return string
     */
    public function senderControllerEdit()
    {
        return $this->senderController().'@edit';
    }
    /**
     * Display the sender controller destroy method
     *
     * @return string
     */
    public function senderControllerDestroy()
    {
        return $this->senderController().'@destroy';
    }

    /**
     * Display the fully qualified classname of this reciepent
     *
     * @return string
     */
    public function classname()
    {
        return __CLASS__;
    }
    public function transformerClass()
    {
        return 'Sirs\Communications\Transformers\SenderTransformer';
    }
}
