<?php
namespace Sirs\Communications\Traits;

trait RecipientTraits
{

    /**
     * Display a listing of the channels associated with this recipient
     *
     * @return array of Channel objects
     */
    public function channels()
    {
        return $this->morphMany('Sirs\Communications\Models\Channel', 'recipient');
    }

    /**
     * Display a listing of the communications associated with this recipient
     *
     * @return Collection of Communication objects
     */
    public function communications()
    {
        return $this->hasManyThrough('Sirs\Communications\Models\CommLog', 'Sirs\Communications\Models\Channel', 'recipient_id', 'channel_id')->where('recipient_type', '=', get_class($this));
    }
    /**
     * Display the recipient base laravel route that you set up
     *
     * @return string
     */
    public function recipientBaseRoute()
    {
        $class = strtolower(join('', array_slice(explode('\\', __CLASS__), -1)));
        return $class.'s';
    }
    /**
     * Display the recipient controller classname
     *
     * @return string
     */
    public function recipientController()
    {
        $class = join('', array_slice(explode('\\', __CLASS__), -1));
        return $class.'Controller';
    }
    /**
     * Display the recipient controller show method
     *
     * @return string
     */
    public function recipientControllerShow()
    {
        return $this->recipientController().'@show';
    }
    /**
     * Display the recipient controller show method
     *
     * @return string
     */
    public function recipientControllerStore()
    {
        return $this->recipientController().'@show';
    }
    /**
     * Display the recipient controller create method
     *
     * @return string
     */
    public function recipientControllerCreate()
    {
        return $this->recipientController().'@create';
    }
    /**
     * Display the recipient controller index method
     *
     * @return string
     */
    public function recipientControllerIndex()
    {
        return $this->recipientController().'@index';
    }
    /**
     * Display the recipient controller update method
     *
     * @return string
     */
    public function recipientControllerUpdate()
    {
        return $this->recipientController().'@update';
    }
    /**
     * Display the recipient controller edit method
     *
     * @return string
     */
    public function recipientControllerEdit()
    {
        return $this->recipientController().'@edit';
    }
    /**
     * Display the recipient controller destroy method
     *
     * @return string
     */
    public function recipientControllerDestroy()
    {
        return $this->recipientController().'@destroy';
    }

    /**
     * Display the fully qualified classname of this reciepent
     *
     * @return string
     */
    public function classname()
    {
        return __CLASS__;
    }
    public function transformerClass()
    {
        return 'Sirs\Communications\Transformers\RecipientTransformer';
    }
}
