<?php namespace Sirs\Communications\Requests;

use App\Http\Requests\Request;
use Sirs\Communications\Models\ChannelType;

class ChannelRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'address'=>'required',
            'recipient_type'=>'required|in:'.implode(',', config('communications.recipientTypes')),
            'recipient_id'=>'required|integer',
            // 'channel_type_id'=>'required|in:'.implode(',', ChannelType::select('id')->get()->toArray()),
        ];
    }
}
