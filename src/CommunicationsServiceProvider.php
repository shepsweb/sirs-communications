<?php namespace Sirs\Communications;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Sirs\Communications\Models\Channel;
use Sirs\Communications\Observers\ChannelObserver;

class CommunicationsServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        $this->app->register(\Spatie\Tags\TagsServiceProvider::class);

        $this->loadViewsFrom(__DIR__.'/Views', 'communications');

        $this->publishes([ __DIR__.'/Config/config.php' => config_path('communications.php') ], 'config');



        $this->publishes([ __DIR__.'/Database/Migrations/' => database_path('/migrations') ], 'migrations');

        $this->publishes([ __DIR__.'/Database/Seeds/' => database_path('/seeds') ], 'seeds');

        include __DIR__.'/routes.php';

        Channel::observe(new ChannelObserver);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
