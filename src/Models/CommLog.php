<?php

namespace Sirs\Communications\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class CommLog extends Model
{
    protected $fillable = [
    'sender_type',
    'sender_id',
    'channel_id',
    'comm_reason_id',
    'comm_status_id',
    'date',
    'notes',
    'updated_by_type',
    'updated_by_id'
  ];
    protected $date = [
    'date'
  ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        if (config('communications.global_scopes') && count(config('communications.global_scopes')) > 0) {
            foreach (config('communications.global_scopes') as $scopeClassName) {
                static::addGlobalScope(new $scopeClassName);
            }
        }
    }

    public function getDates()
    {
        return ['created_at', 'updated_at', 'date'];
    }

    public function channel()
    {
        return $this->belongsTo('Sirs\Communications\Models\Channel');
    }

    public function recipient()
    {
        return $this->channel->recipient();
    }

    public function sender()
    {
        return $this->morphTo();
    }

    public function status()
    {
        return $this->belongsTo('Sirs\Communications\Models\CommunicationStatus', 'comm_status_id');
    }

    public function reason()
    {
        return $this->belongsTo('Sirs\Communications\Models\CommunicationReason', 'comm_reason_id');
    }

    // deprecated
    public function commStatus()
    {
        Log::notice('the commStatus Relationship is deprecated.  Use "status"');

        return $this->belongsTo('Sirs\Communications\Models\CommunicationStatus');
    }

    // deprecated
    public function commReason()
    {
        Log::notice('the commReason Relationship is deprecated.  Use "reason"');

        return $this->belongsTo('Sirs\Communications\Models\CommunicationReason');
    }

    /**
     * relationship to entity that modified this record
     *
     * @return Illuminate\Database\Eloquent\Relations
     **/
    public function updatedBy()
    {
        return $this->morphTo();
    }
}
