<?php


namespace Sirs\Communications\Models;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    use \Spatie\Tags\HasTags;

    protected $table = 'comm_channels';
    protected $fillable = [
        'name',
        'address',
        'recipient_type',
        'recipient_id',
        'channel_type_id',
        'is_primary',
        'notes',
        'is_bad',
        'updated_by_type',
        'updated_by_id'
    ];
    protected $appends  = ['formattedAddress'];

    public function recipient()
    {
        return $this->morphTo();
    }

    public function communications()
    {
        return $this->hasMany('Sirs\Communications\Models\CommLog');
    }

    public function channelType()
    {
        return $this->belongsTo('Sirs\Communications\Models\ChannelType');
    }

    public function scopeShowChannelType($query, $type_id)
    {
        return $query->where('channel_type_id', '=', $type_id);
    }

    public function scopePrimary($query)
    {
        return $query->where('is_primary', '=', 1);
    }

    /**
     * Attempts to json_decode the address attribute.
     *
     * @return mixed object if valid json string or original string
     */
    public function getDecodedAddressAttribute()
    {
        $address = json_decode($this->attributes['address']);
        if (json_last_error() == JSON_ERROR_NONE) {
            return $address;
        }

        return $this->attributes['address'];
    }

    public function scopeType($query, $type)
    {
        return $query->where('channel_type_id', $type);
    }

    public function setAddressAttribute($value)
    {
        $this->attributes['address'] = (is_array($value))
                                            ? $address = json_encode($value)
                                            : $value;
    }

    /**
     * relationship to entity that modified this record
     *
     * @return Illuminate\Database\Eloquent\Relations
     **/
    public function updatedBy()
    {
        return $this->morphTo();
    }
    public function scopeGoodAddress($query)
    {
        return $query->where('is_bad', '=', 0);
    }

    /**
    * Formats address based on old address formatting rules
    * @deprecated
    * @return string
    */
    public function getFormattedAddressAttribute()
    {
        if (preg_match('/;/', $this->address)) {
            return str_replace(';', ' ', $this->address);
        }

        return implode(' ', (array)$this->getDecodedAddressAttribute());
    }

    public function anonymize()
    {
        $faker = \Faker\Factory::create();
        switch ($this->channel_type_id) {
            case 1: // phone
                $this->attributes['address'] = $faker->phoneNumber;
                break;
            case 3: // email
                $this->attributes['address'] = $faker->email;
                break;
            case 4: // Mailing Address
            case 5: // Home Address
                $this->attributes['address'] = json_encode([
                    'street1' => $faker->streetAddress,
                    'street2' => null,
                    'city' => $faker->city,
                    'state' => $faker->state,
                    'zip' => $faker->postcode
                ]);
                break;
            default:
                // do nothing
            break;
        }
        $this->save();
    }

    /**
     * looks at config('communications.addressTypes') to decide if this is an address
     *
     * @return boolean
     **/
    public function getIsAddressTypeAttribute()
    {
        return in_array($this->channel_type_id, config('communications.addressTypes', [4,5]));
    }
}
