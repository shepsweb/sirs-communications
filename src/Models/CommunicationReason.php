<?php 
namespace Sirs\Communications\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;

class CommunicationReason extends Model
{
    use Sluggable;
    use SluggableScopeHelpers;

    protected $table = 'comm_reasons';
    protected $fillable = ['name'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source'=>'name'
            ]
        ];
    }
}
