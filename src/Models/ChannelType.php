<?php
namespace Sirs\Communications\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;

class ChannelType extends Model
{
    use Sluggable;
    use SluggableScopeHelpers;


    protected $table = 'comm_channel_types';
    protected $fillable = ['name'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source'=>'name'
            ]
        ];
    }
}
