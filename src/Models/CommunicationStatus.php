<?php
namespace Sirs\Communications\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;

class CommunicationStatus extends Model
{
    use Sluggable;
    use SluggableScopeHelpers;


    protected $table = 'comm_statuses';
    protected $fillable = ['name'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source'=>'name'
            ]
        ];
    }
}
