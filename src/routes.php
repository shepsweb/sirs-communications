<?php
$auth = (config('communications.auth')) ? config('communications.auth') : [];
// API Routes
Route::group(['middleware'=>$auth], function () {
    $apiPrefix = (config('communications.apiPrefix')) ? config('communications.apiPrefix') : 'api';

    Route::group(['as'=>'api.', 'prefix'=>$apiPrefix], function () {
        Route::resource('communications', '\Sirs\Communications\Http\Controllers\Api\CommunicationController');
        Route::resource(
            'communication_reasons',
            '\Sirs\Communications\Http\Controllers\Api\CommunicationReasonController',
            ['only'=>['index', 'show']]
        );
        Route::resource(
            'communication_statuses',
            '\Sirs\Communications\Http\Controllers\Api\CommunicationStatusController',
            ['only'=>['index', 'show']]
        );
        Route::resource(
            'channel_types',
            '\Sirs\Communications\Http\Controllers\Api\ChannelTypeController',
            ['only'=>['index', 'show']]
        );
        Route::resource('communications', '\Sirs\Communications\Http\Controllers\Api\CommunicationController');
        Route::resource('channels', '\Sirs\Communications\Http\Controllers\Api\ChannelController');
    });

    // Public
    Route::any(
        'channels/createMultiple',
        [
            'as'=>'channels.createMultiple',
            'uses'=> '\Sirs\Communications\Http\Controllers\ChannelController@createMultiple'
        ]
    );
    Route::resource(
        'communications',
        '\Sirs\Communications\Http\Controllers\CommunicationController'
    );
    Route::resource('channels', '\Sirs\Communications\Http\Controllers\ChannelController');
});
