<?php namespace Sirs\Communications\Observers;

use Sirs\Communications\Models\Channel;

class ChannelObserver
{
    public function saved($model)
    {
       
       // make sure there are no other channels of this channel type for this participant that are primary
        if (1 == $model->is_primary) {
            $otherPrimaryChannels = Channel::where('channel_type_id', '=', $model->channel_type_id)
           ->where('recipient_id', '=', $model->recipient_id)
           ->where('recipient_type', '=', $model->recipient_type)
           ->where('is_primary', '=', 1)
           ->where('id', '!=', $model->id)->get();
            //dd($model);
            //dd($otherPrimaryChannels);
            foreach ($otherPrimaryChannels as $ch) {
                $ch->is_primary = 0;
                $ch->save();
            }
        }
        return true;
    }
}
