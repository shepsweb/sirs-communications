<?php

return [
    'recipientTypes' => [
        // add recipient type
        'App\\Participant'
    ],
    'senderTypes'=>[
        // add sender types
        'App\\User'
    ],
    'routeGroupSettings'=>[
        'prefix'=>'api',
        'middleware' => 'auth'
    ],
    'global_scopes'=>[
        # App\Scopes\GlobalCommLogScope::class
    ],
    'addressTypes' => [
        4,
        5
    ],
    'channel_types'=>[
        'phone'=>1,
        'in-person'=>2,
        'email'=>3,
        'mailing-address'=>4,
        'home-address'=>5
    ],
    'statuses'=>[
        'reached'=>1,
        'left-message'=>2,
        'bad-contact-information'=>3,
        'no-english'=>4,
        'no-answer'=>5
    ],
    'reasons'=>[
        'recruitment'=>1,
        'scheduling'=>2,
        'compensation'=>3,
        'contact-check'=>4,
        'birthday'=>5
    ],
];
