<?php
namespace Sirs\Communications\Transformers;

use League\Fractal;
use Sirs\Communications\Models\ChannelType;

class ChannelTypeTransformer extends Fractal\TransformerAbstract
{
    public function transform(ChannelType $ct)
    {
        return [
          'id' => $ct->id,
          'name' => $ct->name,
      ];
    }
}
