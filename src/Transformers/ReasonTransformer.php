<?php
namespace Sirs\Communications\Transformers;

use League\Fractal;
use Sirs\Communications\Models\CommunicationReason;

class ReasonTransformer extends Fractal\TransformerAbstract
{
    public function transform(CommunicationReason $reason)
    {
        return [
          'id' => $reason->id,
          'name' => $reason->name,
      ];
    }
}
