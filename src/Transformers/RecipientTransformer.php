<?php
namespace Sirs\Communications\Transformers;

use League\Fractal;
use Sirs\Communications\Contracts\Recipient;

class RecipientTransformer extends Fractal\TransformerAbstract
{
  
  // protected $availableIncludes = [];
    // protected $defaultIncludes = [ ];

    public function transform(Recipient $recipient)
    {
        return [
          'id' => $recipient->id,
          'fullname' => $recipient->fullName(),
      ];
    }
}
