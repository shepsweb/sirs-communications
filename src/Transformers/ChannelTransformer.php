<?php
namespace Sirs\Communications\Transformers;

use League\Fractal;
use Sirs\Communications\Models\Channel;

class ChannelTransformer extends Fractal\TransformerAbstract
{
  
  // protected $availableIncludes = ['sender'];
    protected $defaultIncludes = [
    'channelType', 'recipient'
  ];

    public function transform(Channel $channel)
    {
        return [
          'id' => $channel->id,
          'name' => $channel->name,
          'recipient_type' => $channel->recipient_type,
          'recipient_id' => $channel->recipient_id,
          'channel_type_id' => $channel->channel_type_id,
          'is_primary' => $channel->is_primary,
          'address' => $channel->address,
          'notes' => $channel->notes,
      ];
    }

    public function includeChannelType(Channel $channel)
    {
        return $this->item($channel->channelType, new ChannelTypeTransformer);
    }
    public function includeRecipient(Channel $channel)
    {
        $classname = $channel->recipient->transformerClass();
        $r = $this->item($channel->recipient, new $classname());
        return $r;
    }
}
