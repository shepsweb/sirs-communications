<?php
namespace Sirs\Communications\Transformers;

use League\Fractal;
use Sirs\Communications\Models\CommunicationStatus;

class StatusTransformer extends Fractal\TransformerAbstract
{
    public function transform(CommunicationStatus $status)
    {
        return [
          'id' => $status->id,
          'name' => $status->name,
      ];
    }
}
