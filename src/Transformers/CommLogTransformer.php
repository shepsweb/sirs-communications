<?php
namespace Sirs\Communications\Transformers;

use League\Fractal;
use Sirs\Communications\Models\CommLog;

class CommLogTransformer extends Fractal\TransformerAbstract
{
    // protected $availableIncludes = ['sender'];
    protected $defaultIncludes = [
    'channel',
    'reason',
    'status',
    'sender'
  ];

    public function transform(CommLog $log)
    {
        return [
          'id' => $log->id,
          'date' => $log->date,
          'sender_id' => $log->sender_id,
          'sender_type' => $log->sender_type,
          'channel_id' => $log->channel_id,
          'comm_reason_id' => $log->comm_reason_id,
          'comm_status_id' => $log->comm_status_id,
          'notes' => $log->notes
      ];
    }

    // public function includeSender(CommLog $log)
    // {
    //     return $this->item($log->sender, new SenderTransformer);
    // }

    public function includeReason(CommLog $log)
    {
        return $this->item($log->reason, new ReasonTransformer);
    }

    public function includeStatus(CommLog $log)
    {
        return $this->item($log->status, new StatusTransformer);
    }

    public function includeChannel(CommLog $log)
    {
        return $this->item($log->channel, new ChannelTransformer);
    }
    public function includeSender(CommLog $log)
    {
        $classname = $log->sender->transformerClass();
        $s = $this->item($log->sender, new $classname());
        return $s;
    }
}
