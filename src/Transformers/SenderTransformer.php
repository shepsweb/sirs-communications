<?php
namespace Sirs\Communications\Transformers;

use League\Fractal;
use Sirs\Communications\Contracts\Sender;

class SenderTransformer extends Fractal\TransformerAbstract
{
  
  // protected $availableIncludes = [];
    // protected $defaultIncludes = [ ];

    public function transform(Sender $sender)
    {
        return [
          'id' => $sender->id,
          'fullname' => $sender->fullName(),
      ];
    }
}
