<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunicationLogsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comm_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sender_type');
            $table->integer('sender_id');
            $table->integer('channel_id')->unsigned();
            $table->integer('comm_reason_id')->unsigned();
            $table->integer('comm_status_id')->unsigned();
            $table->datetime('date');
            $table->mediumText('notes')->nullable();
            $table->morphs('updated_by');
            $table->timestamps();

            $table->index(['sender_type', 'sender_id']);

            $table->foreign('channel_id')->references('id')->on('comm_channels')->onDelete('restrict');
            $table->foreign('comm_reason_id')->references('id')->on('comm_reasons')->onDelete('restrict');
            $table->foreign('comm_status_id')->references('id')->on('comm_statuses')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comm_logs');
    }
}
