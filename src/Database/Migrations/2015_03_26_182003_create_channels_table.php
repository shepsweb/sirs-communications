<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChannelsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comm_channels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('recipient_type');
            $table->integer('recipient_id');
            $table->integer('channel_type_id')->unsigned();
            $table->tinyInteger('is_primary')->default(0);
            $table->string('address');
            $table->mediumText('notes')->nullable();
            $table->morphs('updated_by');
            $table->timestamps();

            $table->foreign('channel_type_id')->references('id')->on('comm_channel_types')->onDelete('restrict');
            $table->index(['recipient_type', 'recipient_id']);
            $table->index(['recipient_type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comm_channels');
    }
}
