<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBadFlagToChannels extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('comm_channels', function(Blueprint $table)
    {
      $table->boolean('is_bad')->default(0)->after('is_primary');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('comm_channels', function(Blueprint $table)
    {
      $table->dropColumn('is_bad');
    });
  }

}
