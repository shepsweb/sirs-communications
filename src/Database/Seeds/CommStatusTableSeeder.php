
<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Sirs\Communications\Models\CommunicationStatus;

class CommStatusTableSeeder extends Seeder
{

  /**
   * Run the database seeds.
   *
   * @return void
   */
    public function run()
    {
        Model::unguard();
        foreach (config('communications.statuses') as $slug => $id) {
            CommunicationStatus::create([
                'id'=>$id,
                'slug'=>$slug,
                'name'=>ucwords(str_replace('-', ' ', $slug))
            ]);
        }
    }
}
