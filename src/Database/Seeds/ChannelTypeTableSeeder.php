<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Sirs\Communications\Models\ChannelType;

class ChannelTypeTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        foreach (config('communications.channel_types') as $slug => $id) {
            ChannelType::create([
                'id'=>$id,
                'slug'=>$slug,
                'name'=>ucwords(str_replace('-', ' ', $slug))
            ]);
        }
    }
}
