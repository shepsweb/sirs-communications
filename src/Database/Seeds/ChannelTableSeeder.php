<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Sirs\Communications\Models\Channel;

class ChannelTableSeeder extends Seeder
{

  /**
   * Run the database seeds.
   *
   * @return void
   */
    public function run()
    {
        Model::unguard();

        Channel::create([
      'name'=>'Cell Phone',
      'recipient_type'=>'App\Participant',
      'recipient_id'=>1,
      'channel_type_id'=>1,
      'is_primary'=>1,
      'address'=>'919-423-4621',
      'notes'=>'',
      'updated_by_type'=>'App\Participant',
      'updated_by_id'=>1,
    ]);

        Channel::create([
      'name'=>'Home Phone',
      'recipient_type'=>'App\Participant',
      'recipient_id'=>1,
      'channel_type_id'=>1,
      'is_primary'=>0,
      'address'=>'919-423-4621',
      'notes'=>'Home phone only usefull after business hours',
      'updated_by_type'=>'App\Participant',
      'updated_by_id'=>1,
    ]);

        Channel::create([
      'name'=>'Bobby\'s Address',
      'recipient_type'=>'App\Participant',
      'recipient_id'=>2,
      'channel_type_id'=>4,
      'is_primary'=>1,
      'address'=> [
        'street1' => '123 Beans St',
        'street2' => '',
        'city' => 'Carrboro',
        'state' => 'NC',
        'zip' => '27510',
      ],
      'updated_by_type'=>'App\Participant',
      'updated_by_id'=>1,
    ]);
    }
}
