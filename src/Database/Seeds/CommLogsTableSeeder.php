<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Sirs\Communications\Models\CommLog;

class CommLogTableSeeder extends Seeder
{

  /**
   * Run the database seeds.
   *
   * @return void
   */
    public function run()
    {
        Model::unguard();

        CommLog::create([
            'sender_type'=>'App\User',
            'sender_id'=>1,
            'channel_id'=>1,
            'comm_reason_id'=>1,
            'comm_status_id'=>1,
            'date'=>'2015-01-01 00:00:00',
            'notes'=>'This is a seeded communication log entry',
            'updated_by_type'=>'App\Participant',
            'updated_by_id'=>1,
        ]);

        CommLog::create([
            'sender_type'=>'App\User',
            'sender_id'=>1,
            'channel_id'=>2,
            'comm_reason_id'=>2,
            'comm_status_id'=>3,
            'date'=>'2015-01-02 00:00:00',
            'notes'=>'This is also seeded communication log entry',
            'updated_by_type'=>'App\Participant',
            'updated_by_id'=>1,
        ]);
    }
}
