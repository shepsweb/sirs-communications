<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Sirs\Communications\Models\CommunicationReason;

class CommunicationReasonTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        foreach (config('communications.reasons') as $slug => $id) {
            CommunicationReason::create([
                'id'=>$id,
                'slug'=>$slug,
                'name'=>ucwords(str_replace('-', ' ', $slug))
            ]);
        }
    }
}
