<?php 
namespace Sirs\Communications\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Sirs\Communications\BasicArraySerializer;
use League\Fractal;

class CommAPIController extends Controller
{
    public function __construct()
    {
        $this->manager = new Fractal\Manager();
        $this->manager->setSerializer(new BasicArraySerializer());
    }

    /**
     * Display a classname of Recipient object that has this classname
     *
     * @return string
     */
    public function returnRecipientObjectName($routeName)
    {
        $classes  = \Config::get('communications.recipientTypes');
        $ret = null;
        foreach ($classes as $klass) {
            $obj = new $klass();
            $obj = $obj->first();

            if ($routeName == $obj->recipientBaseRoute()) {
                $ret = $obj->classname();
            }
        }

        if (is_null($ret)) {
            throw new \Exception("Could not find class that uses route: ".$routeName."\n in ".print_r($classes, true));
        }
        
        return $ret;
    }
}
