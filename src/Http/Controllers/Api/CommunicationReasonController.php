<?php 
namespace Sirs\Communications\Http\Controllers\Api;

use Illuminate\Http\Request;
use Redirect;
use Sirs\Communications\Http\Controllers\Api\CommAPIController;
use Sirs\Communications\Models\CommunicationReason;

class CommunicationReasonController extends CommAPIController
{
  
  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
    public function index(Request $request)
    {
        return CommunicationReason::all();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $reason = CommunicationReason::findOrFail($id);
        return $reason;
    }
}
