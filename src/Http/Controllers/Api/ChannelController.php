<?php 
namespace Sirs\Communications\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use League\Fractal;
use Redirect;
use Response;
use Sirs\Communications\Http\Controllers\Api\CommAPIController;
use Sirs\Communications\Models\Channel;
use Sirs\Communications\Models\ChannelType;
use Sirs\Communications\Requests\ChannelRequest;
use Sirs\Communications\Transformers\ChannelTransformer;

class ChannelController extends CommAPIController
{
    protected $validFilters = [
        'recipient_type',
        'recipient_id',
        'channel_type_id',
        'channel_id',
        'is_primary',
        'is_bad',
        'address',
        'name'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Channel::with('channelType', 'recipient');
        if ($request) {
            foreach ($request->all() as $key => $value) {
                if (in_array($key, $this->validFilters)) {
                    $query->where($key, '=', $value);
                }
            }
        }
        
        $channels = $query->get();
        
        $resource = new Fractal\Resource\Collection($channels, new ChannelTransformer);
        $data = $this->manager->createData($resource)->toArray();
        return Response::json($data, 200);
    }


    // /**
    //  * Display a listing of the resource.
    //  *
    //  * @return Response
    //  */
    // public function index($id = null, Request $request = null)
    // {
        
    // 	if ( !is_null($id ) && is_numeric( $id )) {
    // 		$routeName = $request->segments()[0];
    // 		if ('api' == $routeName) {
    // 			$routeName = $request->segments()[1];
    // 		}
    // 		$objName = $this->returnRecipientObjectName($routeName);
    // 		$recipient = $objName::with('channels')->findOrFail($id);
            
    // 		$channels = $recipient->channels;
    // 	}  else {
    // 		$query = Channel::with('channelType', 'recipient');
    // 		foreach( $request->all() as $key => $value ){
    // 			if( in_array($key, $this->validFilters) ){
    // 				$query->where($key, '=', $value);
    // 			}
    // 		}
    // 		$channels = $query->get();
    // 	}

        
    // 	$resource = new Fractal\Resource\Collection($channels, new ChannelTransformer);
    //    	$data = $this->manager->createData($resource)->toArray();
    //    	return Response::json($data, 200);
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        //return view('channels.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $channel = Channel::create($input);
    
        return $this->show($channel->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $channel = Channel::findOrFail($id);

        $resource = new Fractal\Resource\Item($channel, new ChannelTransformer);
        $data = $this->manager->createData($resource)->toArray();
        return Response::json($data, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($channel_id, Request $request)
    {
        $channel_data = $request->all();
        $channel = Channel::findOrFail($channel_id);
        $channel->update($channel_data);
        
        return $this->show($channel->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $channel = Channel::findOrFail($id);
        $channel->delete();
        
        return Response::json(["deleted"=>'channel id: '.$id], 200);
    }
}
