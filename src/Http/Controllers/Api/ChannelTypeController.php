<?php 
namespace Sirs\Communications\Http\Controllers\Api;

use Illuminate\Http\Request;
use Redirect;
use Sirs\Communications\Http\Controllers\Api\CommAPIController;
use Sirs\Communications\Models\ChannelType;

class ChannelTypeController extends CommAPIController
{
  
  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
    public function index(Request $request)
    {
        return ChannelType::all();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $type = ChannelType::findOrFail($id);
        return $type;
    }
}
