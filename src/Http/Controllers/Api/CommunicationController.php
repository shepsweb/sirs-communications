<?php 
namespace Sirs\Communications\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use League\Fractal;
use Response;
use Sirs\Communications\Http\Controllers\Api\CommAPIController;
use Sirs\Communications\Models\CommLog;
use Sirs\Communications\Transformers\CommLogTransformer;

class CommunicationController extends CommAPIController
{
    protected $validFilters = [
        'recipient_type',
        'recipient_id',
        'sender_type',
        'sender_id',
        'channel_type_id',
        'channel_id',
        'comm_reason_id',
        'comm_status_id',
        'notes',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $params = $request->all();

        $query = CommLog::with('channel', 'channel.recipient', 'sender', 'reason', 'status');
        foreach ($params as $key => $value) {
            switch ($key) {
                case 'recipient_type':
                    $query = $query->whereHas('channel', function ($query) use ($value) {
                        $query->where('recipient_type', '=', $value);
                    });
                    break;
                case 'recipient_id':
                    $query = $query->whereHas('channel', function ($query) use ($value) {
                        $query->where('recipient_id', '=', $value);
                    });
                    break;
                case 'channel_type_id':
                    $query = $query->whereHas('channel', function ($query) use ($value) {
                        $query->where('channel_type_id', '=', $value);
                    });
                    break;
                default:
                    if (in_array($key, $this->validFilters)) {
                        $query->where($key, '=', $value);
                    }
                    break;
            }
        }

        if (isset($params['orderBy'])) {
            $query->orderBy($params['orderBy']);
        }

        if (isset($params['limit'])) {
            $query->take($params['limit']);
            if (isset($params['offset'])) {
                $query->skip($params['offset']);
            }
        }

        $communications = $query->get();
        $resource = new Fractal\Resource\Collection($communications, new CommLogTransformer);
        $data = $this->manager->createData($resource)->toArray();

        return Response::json($data, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $communication = CommLog::create($input);

        return $this->show($communication->id);
        
        // return json_encode(array('store'=>'successful', 'data'=>$communication));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $communication = CommLog::findOrFail($id);
        $resource = new Fractal\Resource\Item($communication, new CommLogTransformer);
        $data = $this->manager->createData($resource)->toArray();
        return Response::json($data, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();
        $communication = CommLog::findOrFail($id);
        $communication->update($input);

        return $this->show($communication->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $communication = CommLog::findOrFail($id);
        $communication->delete();
        
        
        return $this->index();
    }
}
