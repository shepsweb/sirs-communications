<?php 
namespace Sirs\Communications\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Redirect;
use Sirs\Communications\Http\Controllers\Api\CommAPIController;
use Sirs\Communications\Models\Channel;
use Sirs\Communications\Models\CommLog;

class CommunicationController extends CommAPIController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($recipient_id = null)
    {
        if (!is_null($recipient_id)) {
            $route =  \Request::segments()[0];
            $className = $this->returnRecipientObjectName($route);
            $klass = new $className();
            $request = Request::create('/api/'.$route.'/'.$recipient_id.'/communications', 'GET');
        } else {
            $request = Request::create('/api/communications', 'GET');
        }
        

        $response = \Route::dispatch($request)->getContent();
        $arrcommunications = json_decode($response);
        
        $communications = array();
        foreach ($arrcommunications as $communication) {
            $communications[] =  CommLog::findOrFail($communication->id);
        }
        $compact = compact('communications', 'recipient_id');
        if (!is_null($recipient_id)) {
            $recipient = $klass::findOrFail($recipient_id);
            $compact = compact('communications', 'recipient_id', 'recipient');
        }

        
        return view('communications::logs.list', $compact);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create($id = null)
    {
        $commReasons = \Sirs\Communications\Models\CommunicationReason::select('id', 'name')->get()->keyBy('id')->transform(function ($item) {
            return $item->name;
        });
        $commStatuses = \Sirs\Communications\Models\CommunicationStatus::select('id', 'name')->get()->keyBy('id')->transform(function ($item) {
            return $item->name;
        });
        

        if (!is_null($id)) {
            $route =  \Request::segments()[0];
            $className = $this->returnRecipientObjectName($route);
            $klass = new $className();
            $recipient = $klass::findOrFail($id);
            $channelsobj = $recipient->channels;
            $channels = $channelsobj->keyBy('id')->transform(function ($item) {
                return $item->name;
            });
        } else {
            $channels = Channel::select('id', 'name')->get()->keyBy('id')->transform(function ($item) {
                return $item->name;
            });
        }

        $rts = config('communications.senderTypes');
        $senders = [];
        $senderTypes = [];
        foreach ($rts as $rt) {
            $senderTypes[$rt] = explode('\\', $rt)[1];
            $col = $rt::all();
            foreach ($col as $c) {
                $senders[] = ['id' => $c->id, 'name' => $c->full_name, 'class' => explode('\\', $rt)[1]];
            }
        }
        $sender = auth()->user();
        return view('communications::logs.create', compact('commReasons', 'commStatuses', 'channels', 'senderTypes', 'senders', 'sender'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if ($request->get('submit')) {
            $requestapi = Request::create('/api/communications', 'POST', $request->all());
        
            

            $response = \Route::dispatch($requestapi);
            $data = $response->getData();
            
            $comm = CommLog::findOrFail($data->id);
            return Redirect::route('communications.show', [ $comm->id]);
        } else {
            return Redirect::route('communications.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id, $comm_id = null)
    {
        if (!is_null($comm_id)) {
            $actualCommId = $comm_id;
        } else {
            $actualCommId = $id;
        }
        $request = Request::create('/api/communications/'.$actualCommId, 'GET');
        $response = \Route::dispatch($request)->getContent();
        
        $obj = json_decode($response);
        $communication = \Sirs\Communications\Models\CommLog::findOrFail($obj->id);

        return view('communications::logs.detail', compact('communication'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id, $comm_id = null)
    {
        if (!is_null($comm_id)) {
            $route =  \Request::segments()[0];
            $className = $this->returnRecipientObjectName($route);
            $recipient = $className::findOrFail($id);
            
            $channelsobj = $recipient->channels;
            
            $channels = $channelsobj->keyBy('id')->transform(function ($item) {
                return $item->name;
            });
            
            $actualCommId = $comm_id;
        } else {
            $c = CommLog::findOrFail($id);
            $ch = $c->channel;
            
            $recipient = $ch->recipient;
            //$channels = Channel::select('id', 'name')->get()->keyBy('id')->transform(function($item){ return $item->name;});
            $channelsobj = $recipient->channels;
            
            $channels = $channelsobj->keyBy('id')->transform(function ($item) {
                return $item->name;
            });
            
            $actualCommId = $id;
        }

        $request = Request::create('/api/communications/'.$actualCommId, 'GET');
        $response = \Route::dispatch($request)->getContent();
        
        $obj = json_decode($response);

        $commReasons = \Sirs\Communications\Models\CommunicationReason::select('id', 'name')->get()->keyBy('id')->transform(function ($item) {
            return $item->name;
        });
        $commStatuses = \Sirs\Communications\Models\CommunicationStatus::select('id', 'name')->get()->keyBy('id')->transform(function ($item) {
            return $item->name;
        });
        $communication = CommLog::findOrFail($obj->id);


        $rts = config('communications.senderTypes');
        $senders = [];
        $senderTypes = [];
        foreach ($rts as $rt) {
            $senderTypes[$rt] = explode('\\', $rt)[1];
            $col = $rt::all();
            foreach ($col as $c) {
                $senders[] = ['id' => $c->id, 'name' => $c->full_name, 'class' => explode('\\', $rt)[1]];
            }
        }

        $sender = $communication->sender;
        return view('communications::logs.edit', compact('commReasons', 'commStatuses', 'communication', 'channels', 'id', 'comm_id', 'senders', 'senderTypes', 'sender'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        if ($request->get('submit')) {
            $requestapi = Request::create('/api/communications/'.$id, 'POST', $request->all());
        
            

            $response = \Route::dispatch($requestapi);
            
            $data = $response->getData();
            
            $comm = CommLog::findOrFail($data->id);
        } else {
            $comm = CommLog::findOrFail($id);
        }
        return Redirect::route('communications.show', [ $comm->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
