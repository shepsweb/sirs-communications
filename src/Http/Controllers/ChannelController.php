<?php 
namespace Sirs\Communications\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Redirect;
use Route;
use Sirs\Communications\Http\Controllers\Api\CommAPIController;
use Sirs\Communications\Models\Channel;
use Sirs\Communications\Models\ChannelType;

class ChannelController extends CommAPIController
{
    protected $validFilters = [
        'recipient_type',
        'recipient_id',
        'channel_type_id',
        'channel_id',
        'is_primary',
        'is_bad',
        'address',
        'name'
    ];

    public function getRequestData($id)
    {
        $requestData = [];
        if ($id) {
            $requestData['recipient_id'] = $id;
            $routeName = request()->segments()[0];
            $objName = $this->returnRecipientObjectName($routeName);
            $requestData['recipient_type'] = $objName;
        }
        return $requestData;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id = null)
    {
        $requestData = $this->getRequestData($id);

        $query = Channel::with('channelType', 'recipient');
        foreach ($requestData  as $key => $value) {
            if (in_array($key, $this->validFilters)) {
                $query->where($key, '=', $value);
            }
        }
        
        $recipient = null;
        if ($id) {
            $recipient = $requestData['recipient_type']::find($requestData['recipient_id']);
        }

        $channels = $query->get();
        return view('communications::channels.list', compact('channels', 'recipient'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create($id = null, Request $request = null)
    {
        $channelTypes = ChannelType::select('id', 'name')
                            ->get()
                            ->keyBy('id')
                            ->transform(function ($item) {
                                return $item->name;
                            });
        $rts = config('communications.recipientTypes');

        $recipients = [];
        $recipientTypes = [];
        foreach ($rts as $rt) {
            $recipientTypes[$rt] = explode('\\', $rt)[1];
            $col = $rt::all();
            foreach ($col as $c) {
                $recipients[] = ['id' => $c->id, 'name' => $c->fullName(), 'class' => explode('\\', $rt)[1]];
            }
        }
        
        
        if (!empty($id) && !empty($request)) {
            $routeName = $request->segments()[0];
            $recipientType = $this->returnRecipientObjectName($routeName);
            $recipientId = $id;

            $compact = compact('channelTypes', 'recipientTypes', 'recipients', 'recipientType', 'recipientId');
        } else {
            $recipientType = 0;
            $recipientId = 0;
            $compact = compact('channelTypes', 'recipientTypes', 'recipients', 'recipientType', 'recipientId');
        }

        return view('communications::channels.create', $compact);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function createMultiple($id = null, Request $request = null)
    {
        $channelTypes = ChannelType::select('id', 'name')->get()->keyBy('id')->transform(function ($item) {
            return $item->name;
        });
        $rts = config('communications.recipientTypes');

        $recipients = [];
        $recipientTypes = [];
        foreach ($rts as $rt) {
            $recipientTypes[$rt] = explode('\\', $rt)[1];
            $col = $rt::all();
            foreach ($col as $c) {
                $recipients[] = ['id' => $c->id, 'name' => $c->fullName(), 'class' => explode('\\', $rt)[1]];
            }
        }
        
        if (!empty($id) && !empty($request)) {
            $routeName = $request->segments()[0];
            $recipientType = $this->returnRecipientObjectName($routeName);
            $recipientId = $id;
        } else {
            $recipientType = 0;
            $recipientId = 0;
        }
        $compact = compact('channelTypes', 'recipientTypes', 'recipients', 'recipientType', 'recipientId');

        return view('communications::channels.createmultiple', $compact);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if ($request->get('submit')) {
            $arrFields = ['name', 'channel_type_id', 'address', 'is_primary', 'is_bad', 'recipient_type', 'recipient_id', 'notes'];

            $in = $request->all();
            
            for ($i = 1; $i <=3; $i++) {
                $chan = [];
                $found = false;
                foreach ($arrFields as $f) {
                    if (isset($in[$f.$i])) {
                        $chan[$f] = $in[$f.$i];
                        $found = true;
                    }
                }
                if ($found == false && $i == 1) {
                    foreach ($arrFields as $f) {
                        if (isset($in[$f])) {
                            $chan[$f] = $in[$f];
                        }
                    }
                }
                $ct = ChannelType::find($chan['channel_type_id']);
                if (empty($chan['name']) && ($ct->name == 'In Person' || !empty($chan['address']))) {
                    $chan['name'] = $ct->name;
                }
                
                if (!empty($chan['name']) && $chan['name'] != '') {
                    $requestapi = Request::create('/api/channels', 'POST', $chan);
                    $requestapi->replace($chan);
                    $request->replace($chan);
                    $response = \Route::dispatch($requestapi);
                    $data = $response->getData();
                }
                if ($found == false) {
                    break;
                    // escape hatch so it does not redo if only entering 1 channel
                }
            }
        
            
            //$return = json_decode($response->getData());
            
            //return Redirect::route('channels.show', [$data->id]);
        } else {
            // hit cancel
            return Redirect::route('channels.index');
        }
        //else {
        
        $recipientClass = $chan['recipient_type'];
        $recipient =  $recipientClass::findOrFail($chan['recipient_id']);
        return Redirect::route($recipient->recipientBaseRoute() .'.channels.index', array($recipient->id));
        
            
        
        //}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id, $chan_id = null)
    {
        if (!is_null($chan_id)) {
            $actualChanId = $chan_id;
            $route =  \Request::segments()[0];
            $className = $this->returnRecipientObjectName($route);
            $recipient = $className::findOrFail($id);
        } else {
            $actualChanId = $id;
        }
        $request = Request::create('/api/channels/'.$actualChanId, 'GET');
        $response = \Route::dispatch($request)->getContent();
        
        $channelobj = json_decode($response);
        $channel = Channel::findOrFail($channelobj->id);

        if (!is_null($chan_id)) {
            $compact = compact('channel', 'id', 'chan_id', 'recipient');
        } else {
            $compact = compact('channel', 'id', 'chan_id');
        }

        
        return view('communications::channels.detail', $compact);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id, $chan_id = null)
    {
        if (!is_null($chan_id)) {
            $actualChanId = $chan_id;
        } else {
            $actualChanId = $id;
        }
        $request = Request::create('/api/channels/'.$actualChanId, 'GET');
        $response = \Route::dispatch($request)->getContent();
        
        $channelobj = json_decode($response);
        $channelTypes = ChannelType::select('id', 'name')->get()->keyBy('id')->transform(function ($item) {
            return $item->name;
        });
        $rts = config('communications.recipientTypes');
        $recipients = [];
        $recipientTypes = [];
        foreach ($rts as $rt) {
            $recipientTypes[$rt] = explode('\\', $rt)[1];
            $col = $rt::all();
            foreach ($col as $c) {
                $recipients[] = ['id' => $c->id, 'name' => $c->fullName(), 'class' => explode('\\', $rt)[1]];
            }
        }
        $channel = Channel::findOrFail($channelobj->id);

        $recipientType = 0;
        $recipientId = 0;
        return view('communications::channels.edit', compact('channelTypes', 'channel', 'id', 'chan_id', 'recipientTypes', 'recipients', 'recipientType', 'recipientId'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        if ($request->get('submit')) {
            $requestapi = Request::create('/api/channels/'.$id, 'POST', $request->all());
            

            $responseapi =  Route::dispatch($requestapi);
            

            $data = $responseapi->getData();
            
            
            $chan = Channel::findOrFail($data->id);
        } else {
            $chan = Channel::findOrFail($id);
        }
        return Redirect::route('channels.show', [ $chan->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
