<?php 
namespace Sirs\Communications\Contracts;

interface Recipient
{
    public function fullName();
    public function channels();

    /**
     * Display a listing of the communications associated with this recipient
     *
     * @return Collection of Communication objects
     */
    public function communications();
    /**
     * Display the recipient base laravel route that you set up
     *
     * @return string
     */
    public function recipientBaseRoute();
    /**
     * Display the recipient controller classname
     *
     * @return string
     */
    public function recipientController();
    /**
     * Display the recipient controller show method
     *
     * @return string
     */
    public function recipientControllerShow();
    /**
     * Display the recipient controller show method
     *
     * @return string
     */
    public function recipientControllerStore();
    /**
     * Display the recipient controller create method
     *
     * @return string
     */
    public function recipientControllerCreate();
    /**
     * Display the recipient controller index method
     *
     * @return string
     */
    public function recipientControllerIndex();
    /**
     * Display the recipient controller update method
     *
     * @return string
     */
    public function recipientControllerUpdate();
    /**
     * Display the recipient controller edit method
     *
     * @return string
     */
    public function recipientControllerEdit();
    /**
     * Display the recipient controller destroy method
     *
     * @return string
     */
    public function recipientControllerDestroy();

    /**
     * Display the fully qualified classname of this reciepent
     *
     * @return string
     */
    public function classname();
}
