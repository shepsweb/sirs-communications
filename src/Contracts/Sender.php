<?php namespace Sirs\Communications\Contracts;

interface Sender
{
    public function fullName();
    

    /**
     * Display a listing of the communications associated with this sender
     *
     * @return Collection of Communication objects
     */
    public function communications();

    /**
     * Display the sender base laravel route that you set up
     *
     * @return string
     */
    public function senderBaseRoute();

    /**
     * Display the sender controller classname
     *
     * @return string
     */
    public function senderController();

    /**
     * Display the sender controller show method
     *
     * @return string
     */
    public function senderControllerShow();

    /**
     * Display the sender controller show method
     *
     * @return string
     */
    public function senderControllerStore();

    /**
     * Display the sender controller create method
     *
     * @return string
     */
    public function senderControllerCreate();

    /**
     * Display the sender controller index method
     *
     * @return string
     */
    public function senderControllerIndex();

    /**
     * Display the sender controller update method
     *
     * @return string
     */
    public function senderControllerUpdate();

    /**
     * Display the sender controller edit method
     *
     * @return string
     */
    public function senderControllerEdit();

    /**
     * Display the sender controller destroy method
     *
     * @return string
     */
    public function senderControllerDestroy();

    /**
     * Display the fully qualified classname of this reciepent
     *
     * @return string
     */
    public function classname();
}
