@extends(config('communications.layout_template', 'app'))

@section('content')
<style>
   table tbody tr:hover td {
    cursor: pointer;   
    background-color: #DDD;
  }
  </style>
  <h1>Communications
  @if( isset($recipient) )
    for {{ $recipient->fullName() }}</h1>
    <a href="{{ route( $recipient->recipientBaseRoute().'.communications.create', [$recipient->id]) }}" class="btn btn-default pull-right">Add Communication</a>
  @else
    </h1>
     <a href="{{ route('communications.create') }}" class="btn btn-default pull-right">Add Communication</a>

  @endif
  
  <table class="table">
    <tr>
      <th>Sender</th>
      <th>Recipient</th>
      <th>Channel</th>
      <th>Date</th>
      <th>Reason</th>
      <th>Status</th>
 
    </tr>
    @foreach($communications as $communication)
      <tr class="clickable-row" data-href='{{ route( $communication->channel->recipient->recipientBaseRoute().'.communications.show' , [$communication->channel->recipient_id, $communication->id]) }}'>
        <td>{{ $communication->sender->full_name }}</td>
        <td>
            <a href="{{ action($communication->channel->recipient->recipientControllerShow(), [$communication->channel->recipient_id]) }}">
              {{$communication->channel->recipient->fullName()}}
            </a>
        </td>
        <td>
          <a href="{{ route($communication->channel->recipient->recipientBaseRoute().'.channels.show', [$communication->channel->recipient_id,$communication->channel->id]) }}">
              {{$communication->channel->name}}
          </a>
        </td>
        <td>{{ $communication->date }}</td>
        <td>{{ $communication->reason->name }}</td>
        <td>{{ $communication->status->name }}</td>
      </tr>
    @endforeach
  </table>

  @if( isset($recipient) )
     <a href="{{ action($recipient->recipientControllerShow(), [$recipient->id]) }}" class="btn btn-default pull-right">Back to Recipient</a>
  @endif
@endsection

@push('scripts')
  <script>
    jQuery(document).ready(function($) {
        $(".clickable-row").click(function() {
            window.document.location = $(this).data("href");
        });
    });

  </script>
@endpush