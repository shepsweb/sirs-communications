<script src="/bower_components/moment/moment.js" ></script>
<script src="/bower_components/jquery.filthypillow/jquery.filthypillow.js" ></script>
<link rel="stylesheet" href="/bower_components/jquery.filthypillow/jquery.filthypillow.css" />
  <div class="form-group">
    {!! Form::label('sender_type', 'Sender', ['class'=>'form-label col-md-2 text-right']) !!}
    <div class="col-md-8 form-inline">
      {!! Form::select('sender_type', $senderTypes , null, ['class'=>'form-control', 'onchange'=>'listSenders()']) !!}
      {!! Form::select('sender_id', [], null, ['class'=>'form-control']) !!}
    </div>
  </div>

<div class="form-group">
  {!! Form::label('channel_id', 'Channel:', ['class'=>'form-label col-md-2 text-right']) !!}
  {!! HTML::linkRoute('channels.create', 'Add Channel', null, ['class'=>'btn col-md-2']) !!}
  <div class="col-md-6 form-inline">
    {!! Form::select('channel_id', $channels, null, ['class'=>'form-control']) !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('comm_reason_id', 'Reason:', ['class'=>'form-label col-md-2 text-right']) !!}
  <div class="col-md-8 form-inline">
    {!! Form::select('comm_reason_id', $commReasons, null, ['class'=>'form-control']) !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('date', 'Date:', ['class'=>'form-label col-md-2 text-right']) !!}
  <div class="col-md-8 form-inline">
    {!! Form::text( 'date', null , ['class'=>'form-control', 'placeholder'=>'12/31/2000']) !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('comm_status_id', 'Status:', ['class'=>'form-label col-md-2 text-right']) !!}
  <div class="col-md-8 form-inline">
    {!! Form::select('comm_status_id', $commStatuses, null, ['class'=>'form-control']) !!}
  </div>
</div>


<div class="form-group">
  {!! Form::label('notes', 'Notes', ['class'=>'form-label col-md-2 text-right']) !!}
  <div class="col-md-8 form-inline">
    {!! Form::textarea('notes', null, ['class'=>'form-control']) !!}
  </div>
</div>
<div class="form-group">
  <div class="col-md-offset-2 col-md-10">
    <input type="submit" name="cancel" class="btn" value="Cancel">
    <input type="submit" name="submit" class="btn btn-primary" value="{{$submitButtonText}}">
  </div>
</div>
<script>
  var senderList = {!! json_encode( $senders ) !!};

  var previousSender = {{ isset($sender) ? $sender->id : 0 }};
  var previousSenderType = '{{ isset($sender) ?  str_replace( '\\', '\\\\',get_class($sender)) : 0 }}';
  function listSenders() {
    var className = $('#sender_type option:selected' ).text();
     $('[name="sender_id"]').empty();
    var len = senderList.length;
    for( i=0; i<len; i++) {
      if ( senderList[i].class == className ) {
        var selected = '';
        if ( senderList[i].id == previousSender && previousSenderType == 'App\\'+ className) {
          selected = ' selected=selected ';
        } else {
          selected = '  ';
        }
        $('[name="sender_id"]')
          .append('<option value="'+ senderList[i].id +'" '+ selected +'>' + senderList[i].name + '</option>');
      }
    }

  }
  listSenders();
  if ( $('#date').val() === '' ) {
    $('#date').val( '{!! \Carbon\Carbon::now() !!}' );
  }


  var $fp = $( "#date" );
    $fp.filthypillow( { 
    initialDateTime: function( m ) {
      return moment($('#date').val(), 'YYYY-MM-DD HH:mm:ss');
    }
       
    } );
      
 
  $fp.on( "focus", function( ) {
    $fp.filthypillow( "show" );
  } );
  $fp.on( "fp:save", function( e, dateObj ) {
    $fp.val( dateObj.format( "YYYY-MM-DD HH:mm:ss" ) );
    $fp.filthypillow( "hide" );
  } );

</script>