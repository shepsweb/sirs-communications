@extends(config('communications.layout_template', 'app'))

@section('content')

  <h1>Create new Communication</h1>

  {!! Form::open(['route'=>'communications.store', 'method'=>'post']) !!}

    @include('communications::logs._form', ['submitButtonText'=>'Create', 'senders', 'senderTypes', 'sender'])
  
  {!! Form::close() !!}
  
  @include('communications::errors.list')

@endsection