@extends('app')@extends(config('communications.layout_template', 'app'))

@section('content')

  <h1>Edit {{ $communication->channel->name }} for {{$communication->channel->recipient->fullName()}}</h1>

  {!! Form::model($communication, ['route'=>['communications.update', $id ], 'method'=>'PATCH']) !!}

    @include('communications::logs._form', ['submitButtonText'=>'Save', 'senders', 'senderTypes', 'sender'])
  
  {!! Form::close() !!}
  
  @include('communications::errors.list')

@endsection