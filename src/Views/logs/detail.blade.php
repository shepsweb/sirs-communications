@extends(config('communications.layout_template', 'app'))

@section('content')
  <h1>{{$communication->channel->recipient->fullName()}}</h1>
    <a href="{{ route('communications.edit', [$communications->id]) }}" class="btn btn-default pull-right">Edit</a>  
  <dl class="dl-horizontal">
    <dt>Channel:</dt>
    <dd>{{$communication->channel->name}}</dd>

    <dt>Reason:</dt>
    <dd>{{ $communication->reason->name }}</dd>

    <dt>Status:</dt>
    <dd>{{ $communication->status->name }}</dd>

     <dt>Date:</dt>
    <dd>{{ $communication->date }}</dd>

    <dt>Notes:</dt>
    <dd>{{ $communication->notes }}</dd>

  </dl>
  <a href="{{ route($communication->channel->recipient->recipientControllerShow(), [$communication->channel->recipient_id]) }}" class="btn btn-default pull-right">
    Back to Recipient
  </a>
  <a href="{{ route($communication->channel->recipient->recipientBaseRoute().'.communications.show', [$communication->channel->recipient_id]) }}" class="btn btn-default pull-right">
    Back to List
  </a>  
@endsection