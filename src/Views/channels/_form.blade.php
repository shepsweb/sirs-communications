<div class="form-group">
  {!! Form::label('name', 'Name:', ['class'=>'form-label col-md-3 text-right']) !!}
  <div class="col-md-8 form-inline">
    {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Name']) !!}
  </div>
</div>
<div class="form-group">
  {!! Form::label('channel_type_id', 'Type:', ['class'=>'form-label col-md-3 text-right']) !!}
  <div class="col-md-8 form-inline">
    {!! Form::select('channel_type_id', $channelTypes, null, ['class'=>'form-control', 'onchange'=>'createAddressElements(this.id)' ]) !!}
  </div>
</div>
<div class="form-group">
  {!! Form::label('address', 'Address', ['class'=>'form-label col-md-3 text-right']) !!}
  <div class="col-md-8" id='divAddress'>
    {!! Form::text('address', null, ['class'=>'form-control', 'placeholder'=>'' ]) !!}
  </div>
</div>
<div class="form-group">
  {!! Form::label('is_primary', 'Primary', ['class'=>'form-label col-md-3 text-right']) !!}
  <div class="col-md-8 form-inline">
    {!! Form::checkbox('is_primary', '1', false, ['class'=>'form-control']) !!}
  </div>
</div>
@if( !isset($recipient) )
  <div class="form-group">
    {!! Form::label('recipient_type', 'Recipient', ['class'=>'form-label col-md-3 text-right']) !!}
    <div class="col-md-8 form-inline">
      {!! Form::select('recipient_type', $recipientTypes ,null, ['class'=>'form-control', 'onchange'=> 'listRecipients()']) !!}
      {!! Form::select('recipient_id', [], null, ['class'=>'form-control']) !!}
    </div>
  </div>
@endif
<div class="form-group">
  {!! Form::label('notes', 'Notes', ['class'=>'form-label col-md-3 text-right']) !!}
  <div class="col-md-8 form-inline">
    {!! Form::textarea('notes', null, ['class'=>'form-control']) !!}
  </div>
</div>
<div class="form-group">
  <div class="col-md-offset-2 col-md-10">
    <input type="submit" name="cancel" class="btn" value="Cancel">
    <input type="submit" name="submit" class="btn btn-primary" value="{{$submitButtonText}}">
  </div>
</div>

@push('scripts')
<script>
  var recipientList = {!! json_encode( $recipients ) !!};
</script>
<script>
  var previousRecipient = {{ isset($channel) ? $channel->recipient_id : $recipientId }};
 // var previousRecipientType = '{{ isset($channel) ? $channel->recipient_type : 0 }}';
  var previousRecipientType = '{{ isset($channel) ? str_replace( '\\', '\\\\', $channel->recipient_type) : str_replace( '\\', '\\\\',$recipientType) }}';
  if (previousRecipientType != 0) {
    
    $('[name=recipient_type]').val(previousRecipientType);
  }
  function listRecipients() {
    var className = $('#recipient_type option:selected' ).text();
     $('[name="recipient_id"]').empty();
    var len = recipientList.length;
    for( i=0; i<len; i++) {
      if ( recipientList[i].class == className ) {
        var selected = '';
        if ( recipientList[i].id == previousRecipient && previousRecipientType == 'App\\'+ className) {
          selected = ' selected=selected ';
        } else {
          selected = '  ';
        }
        $('[name="recipient_id"]')
          .append('<option value="'+ recipientList[i].id +'" ' + selected + '>' + recipientList[i].name + '</option>');
      }
    }

  }
  listRecipients();

  function setAddress( arrAddressElements ) {
    max = arrAddressElements.length;
    $('#address').val( '' );
    var text = $('#address').val();
    for( var i=0; i< max; i++ ) {      
      text += $(arrAddressElements[i]).val()+';';
    }
    
    $('#address').val( text.substring(0, text.length - 1)); // remove trailing ;
  }

  function initAddress( obj ) {
    var text = $(obj).val();

    console.log(text);
    arr =  text.split( ';' );
    $('#street1').val(arr[0]);
    $('#street2').val(arr[1]);
    $('#city').val(arr[2]);
    $('#state').val(arr[3]);
    $('#zip').val(arr[4]);

    if (arr.length == 1) {
      address = JSON.parse(text);
      $('#street1').val(address.street_1);
      $('#street2').val(address.street_2);
      $('#city').val(address.city);
      $('#state').val(address.state);
      $('#zip').val(address.zip);
    }


  }

  function createAddressElements( objId ) {
    // look at channel_type, create appropriate inputs with onchange ->feed to 'address'
    text = $("#"+objId+" option:selected").text();
    if (typeof document.getElementById("contAddress") != "undefined") {
        $('#contAddress').empty();
      }
    if ( text == 'Mailing Address' || text == 'Street Address' || text == 'Work Address' || text == 'Home Address') {
      // hide address
      // create address1, address2, city, state, zip     prefill with address split on ;
      if (typeof document.getElementById("contAddress") != "undefined") {
        $('#contAddress').empty();
      }
      $('#address').hide();
      $('#divAddress').append('<div id="contAddress"></div>');
      $('#contAddress')
          .append('<input type="text" id="street1" onchange=\'setAddress([$("#street1"), $("#street2"), $("#city"), $("#state"), $("#zip")])\' class="form-control" placeholder="street1" /> ')
          .append('<input type="text" id="street2" onchange=\'setAddress([$("#street1"), $("#street2"), $("#city"), $("#state"), $("#zip")])\' class="form-control" placeholder="street2" /> ')
          .append('<input type="text" id="city" onchange=\'setAddress([$("#street1"), $("#street2"), $("#city"), $("#state"), $("#zip")])\' class="form-control" placeholder="city" /> ')
          .append('<input type="text" id="state" onchange=\'setAddress([$("#street1"), $("#street2"), $("#city"), $("#state"), $("#zip")])\' class="form-control" placeholder="state" /> ')
          .append('<input type="text" id="zip" onchange=\'setAddress([$("#street1"), $("#street2"), $("#city"), $("#state"), $("#zip")])\' class="form-control" placeholder="zip" /> ')
          ; 
      initAddress( $('#address') );
      
    } else {
      // can use address as normal, so do nothing
      $('#address').show();
    }


  }
  

$(document).ready(function() {
    createAddressElements( 'channel_type_id' );
});
</script>
@endpush