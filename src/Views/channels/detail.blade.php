@extends(config('communications.layout_template', 'app'))

@section('content')
  
  <h1>
    {{$channel->recipient->fullName() }} - {{$channel->name}}
    <a href="{{ route('channels.edit', [ $channel->id]) }}" class="btn btn-default pull-right">Edit</a>
  </h1>
  <dl class="dl-horizontal">
    <dt>Type:</dt>
    <dd>{{$channel->channelType->name}}</dd>

    <dt>Primary:</dt>
    <dd>{{ ($channel->is_primary) ? 'yes' : 'no' }}</dd>

    <dt>Address:</dt>
    <dd>{{ $channel->formattedAddress }}</dd>

    <dt>Notes:</dt>
    <dd>{{ $channel->notes }}</dd>

  </dl>

      <a href="{{ action( $channel->recipient->recipientControllerShow(), ['id'=>$channel->recipient_id]) }}" class="btn btn-default btn-xs">
        Back to Recipient
      </a>
      <a href="{{ route($channel->recipient->recipientBaseRoute().'.channels.index', [$channel->recipient_id]) }}" class="btn btn-default btn-xs">
        Back to Channels
      </a>

@endsection