@extends(config('communications.layout_template', 'app'))

@section('content')

  <h1>Create new channel</h1>

  {!! Form::open(['route'=>'channels.store', 'method'=>'post']) !!}

    @include('communications::channels._form', ['submitButtonText'=>'Create', 'recipient', 'recipientTypes', 'recipientType', 'recipientId'])
  
  {!! Form::close() !!}
  
  @include('communications::errors.list')

@endsection