@extends(config('communications.layout_template', 'app'))

@section('content')

  <h1>Edit {{ $channel->name }} for {{$channel->recipient->fullName()}}</h1>

  {!! Form::model($channel, ['route'=>['channels.update', $id ], 'method'=>'PATCH']) !!}

    @include('communications::.channels._form', ['submitButtonText'=>'Save', 'recipient', 'recipientTypes'])
  
  {!! Form::close() !!}
  
  @include('communications::errors.list')

@endsection