@extends(config('communications.layout_template', 'app'))

@section('content')

  <h1>Create multiple channels</h1>

  {!! Form::open(['route'=>'channels.store', 'method'=>'post']) !!}

   
<div class="form-group">
  {!! Form::label('name1', 'Name:', ['class'=>'form-label col-md-3 text-right']) !!}
  <div class="col-md-8 form-inline">
    {!! Form::text('name1', null, ['class'=>'form-control', 'placeholder'=>'Name']) !!}
  </div>
</div>
<div class="form-group">
  {!! Form::label('channel_type_id1', 'Type:', ['class'=>'form-label col-md-3 text-right']) !!}
  <div class="col-md-8 form-inline">
    {!! Form::select('channel_type_id1', $channelTypes, null, ['class'=>'form-control', 'onchange'=>'createAddressElements(this.id, 1)']) !!}
  </div>
</div>
<div class="form-group">
  {!! Form::label('address1', 'Address', ['class'=>'form-label col-md-3 text-right']) !!}
  <div class="col-md-8" id='divAddress1'>
    {!! Form::text('address1', null, ['class'=>'form-control', 'placeholder'=>'']) !!}
  </div>
</div>
<div class="form-group">
  {!! Form::label('is_primary1', 'Primary', ['class'=>'form-label col-md-3 text-right']) !!}
  <div class="col-md-8 form-inline">
    {!! Form::checkbox('is_primary1', '1', false, ['class'=>'form-control']) !!}
  </div>
</div>
@if( !isset($recipient) )
  <div class="form-group">
    {!! Form::label('recipient_type1', 'Recipient', ['class'=>'form-label col-md-3 text-right']) !!}
    <div class="col-md-8 form-inline">
      {!! Form::select('recipient_type1', $recipientTypes ,null, ['class'=>'form-control', 'onchange'=> 'listRecipients()']) !!}
      {!! Form::select('recipient_id1', [], null, ['class'=>'form-control', 'onchange'=>'setRepId(this)']) !!}
    </div>
  </div>
@endif
<div class="form-group">
  {!! Form::label('notes1', 'Notes', ['class'=>'form-label col-md-3 text-right']) !!}
  <div class="col-md-8 form-inline">
    {!! Form::textarea('notes1', null, ['class'=>'form-control']) !!}
  </div>
</div>

<div class="form-group row" >
</div>
<div class="form-group row" >
</div>
<div class="form-group row" >
</div>

<div class="form-group" >
	<div class="form-group">
	  {!! Form::label('name2', 'Name:', ['class'=>'form-label col-md-3 text-right']) !!}
	  <div class="col-md-8 form-inline">
	    {!! Form::text('name2', null, ['class'=>'form-control', 'placeholder'=>'Name']) !!}
	  </div>
	</div>
	<div class="form-group">
	  {!! Form::label('channel_type_id2', 'Type:', ['class'=>'form-label col-md-3 text-right']) !!}
	  <div class="col-md-8 form-inline">
	    {!! Form::select('channel_type_id2', $channelTypes, null, ['class'=>'form-control', 'onchange'=>'createAddressElements(this.id, 2)']) !!}
	  </div>
	</div>
	<div class="form-group">
	  {!! Form::label('address2', 'Address', ['class'=>'form-label col-md-3 text-right']) !!}
	  <div class="col-md-8" id='divAddress2'>
	    {!! Form::text('address2', null, ['class'=>'form-control', 'placeholder'=>'']) !!}
	  </div>
	</div>
	<div class="form-group">
	  {!! Form::label('is_primary2', 'Primary', ['class'=>'form-label col-md-3 text-right']) !!}
	  <div class="col-md-8 form-inline">
	    {!! Form::checkbox('is_primary2', '1', false, ['class'=>'form-control']) !!}
	  </div>
	</div>
	@if( !isset($recipient) )
	  <div class="form-group" style='display: none;'>
	    {!! Form::label('recipient_type2', 'Recipient', ['class'=>'form-label col-md-3 text-right']) !!}
	    <div class="col-md-8 form-inline">
	      {!! Form::text('recipient_type2', '' ,null, ['class'=>'form-control']) !!}
	      {!! Form::text('recipient_id2', '', null, ['class'=>'form-control']) !!}
	    </div>
	  </div>
	@endif
	<div class="form-group">
	  {!! Form::label('notes2', 'Notes', ['class'=>'form-label col-md-3 text-right']) !!}
	  <div class="col-md-8 form-inline">
	    {!! Form::textarea('notes2', null, ['class'=>'form-control']) !!}
	  </div>
	</div>
</div>

<div class="form-group row" >
</div>
<div class="form-group row" >
</div>
<div class="form-group row" >
</div>


<div class="form-group">
	<div class="form-group">
	  {!! Form::label('name3', 'Name:', ['class'=>'form-label col-md-3 text-right']) !!}
	  <div class="col-md-8 form-inline">
	    {!! Form::text('name3', null, ['class'=>'form-control', 'placeholder'=>'Name']) !!}
	  </div>
	</div>
	<div class="form-group">
	  {!! Form::label('channel_type_id3', 'Type:', ['class'=>'form-label col-md-3 text-right']) !!}
	  <div class="col-md-8 form-inline">
	    {!! Form::select('channel_type_id3', $channelTypes, null, ['class'=>'form-control', 'onchange'=>'createAddressElements(this.id, 3)']) !!}
	  </div>
	</div>
	<div class="form-group">
	  {!! Form::label('address3', 'Address', ['class'=>'form-label col-md-3 text-right']) !!}
	  <div class="col-md-8" id='divAddress3'>
	    {!! Form::text('address3', null, ['class'=>'form-control', 'placeholder'=>'']) !!}
	  </div>
	</div>
	<div class="form-group">
	  {!! Form::label('is_primary3', 'Primary', ['class'=>'form-label col-md-3 text-right']) !!}
	  <div class="col-md-8 form-inline">
	    {!! Form::checkbox('is_primary3', '1', false, ['class'=>'form-control']) !!}
	  </div>
	</div>
	@if( !isset($recipient) )
	  <div class="form-group"  style='display: none;'>
	    {!! Form::label('recipient_type3', 'Recipient', ['class'=>'form-label col-md-3 text-right']) !!}
	    <div class="col-md-8 form-inline">
	      {!! Form::text('recipient_type3', '' ,null, ['class'=>'form-control']) !!}
	      {!! Form::text('recipient_id3', '', null, ['class'=>'form-control']) !!}
	    </div>
	  </div>
	@endif
	<div class="form-group">
	  {!! Form::label('notes3', 'Notes', ['class'=>'form-label col-md-3 text-right']) !!}
	  <div class="col-md-8 form-inline">
	    {!! Form::textarea('notes3', null, ['class'=>'form-control']) !!}
	  </div>
	</div>


</div>



<div class="form-group">
  <div class="col-md-offset-2 col-md-10">
    <input type="submit" name="cancel" class="btn" value="Cancel">
    <input type="submit" name="submit" class="btn btn-primary" value="Create">
  </div>
</div>
<script>
  var recipientList = {!! json_encode( $recipients ) !!};
</script>
<script>
  var previousRecipient = {{ isset($channel) ? $channel->recipient_id : $recipientId }};
 // var previousRecipientType = '{{ isset($channel) ? $channel->recipient_type : 0 }}';
  var previousRecipientType = '{{ isset($channel) ? str_replace( '\\', '\\\\', $channel->recipient_type) : str_replace( '\\', '\\\\',$recipientType) }}';
  if (previousRecipientType != 0) {
    
    $('[name=recipient_type1]').val(previousRecipientType);
    $('[name=recipient_type2]').val(previousRecipientType);
    $('[name=recipient_type3]').val(previousRecipientType);
  }
  function listRecipients() {
    var className = $('#recipient_type1 option:selected' ).text();
     $('[name="recipient_id1"]').empty();
     $('[name=recipient_type2]').val($('[name=recipient_type1]').val());
     $('[name=recipient_type3]').val($('[name=recipient_type1]').val());
     $('[name="recipient_id2"]').empty();
	 $('[name="recipient_id3"]').empty();
    var len = recipientList.length;
    for( i=0; i<len; i++) {
      if ( recipientList[i].class == className ) {
        var selected = '';
        var boolSelected = false;
        if ( recipientList[i].id == previousRecipient && previousRecipientType == 'App\\'+ className) {
          selected = ' selected=selected ';
          boolSelected = true;
        } else {
          selected = '  ';
          boolSelected = false;
        }
        $('[name="recipient_id1"]')
          .append('<option value="'+ recipientList[i].id +'" ' + selected + '>' + recipientList[i].name + '</option>');
	    if ( boolSelected == true ) {
	      	$('[name="recipient_id2"]').val(recipientList[i].id);
	      	$('[name="recipient_id3"]').val(recipientList[i].id);
	      	
	    }
          
      }
    }

  }
  function setRepId( obj ) {
  	$('[name="recipient_id2"]').val($(obj).val()); 
  	$('[name="recipient_id3"]').val($(obj).val());
  	//$('[name=recipient_type2]').val(previousRecipientType);
   // $('[name=recipient_type3]').val(previousRecipientType);
  }
  listRecipients();

/*
  	$('#recipient_id2').val($('#recipient_id1 option:selected' ).val()); 
  	$('#recipient_id3').val($('#recipient_id1 option:selected' ).val());
  	$('#recipient_type2').val($('#recipient_type1 option:selected' ).val());
    $('#recipient_type3').val($('#recipient_type1 option:selected' ).val());
*/

 function setAddress( arrAddressElements, elemId ) {
    max = arrAddressElements.length;
    $('#address'+elemId).val( '' );
    var text = $('#address'+elemId).val();
    for( var i=0; i< max; i++ ) {      
      text += $(arrAddressElements[i]).val()+';';
    }
    
    $('#address'+elemId).val( text.substring(0, text.length - 1)); // remove trailing ;
  }

  function initAddress( obj, elemId ) {
    var text = $(obj).val();
    arr =  text.split( ';' );
    $('#street1_'+elemId).val(arr[0]);
    $('#street2_'+elemId).val(arr[1]);
    $('#city_'+elemId).val(arr[2]);
    $('#state_'+elemId).val(arr[3]);
    $('#zip_'+elemId).val(arr[4]);

  }

  function createAddressElements( objId, elemId ) {
    // look at channel_type, create appropriate inputs with onchange ->feed to 'address'
    text = $("#"+objId+" option:selected").text();
    if (typeof document.getElementById("contAddress") != "undefined") {
        $('#contAddress').empty();
      }
    if ( text == 'Mailing Address' || text == 'Street Address' || text == 'Work Address' || text == 'Home Address') {
      // hide address
      // create address1, address2, city, state, zip     prefill with address split on ;
      if (typeof document.getElementById("contAddress") != "undefined") {
        $('#contAddress').empty();
      }
      $('#address'+elemId).hide();
      $('#divAddress'+elemId).append('<div id="contAddress'+elemId+'"></div>');
      $('#contAddress'+elemId)
          .append('<input type="text" id="street1_'+elemId+'" onchange=\'setAddress([$("#street1_'+elemId+'"), $("#street2_'+elemId+'"), $("#city_'+elemId+'"), $("#state_'+elemId+'"), $("#zip_'+elemId+'")], '+elemId+')\' class="form-control" placeholder="street1" /> ')
          .append('<input type="text" id="street2_'+elemId+'" onchange=\'setAddress([$("#street1_'+elemId+'"), $("#street2_'+elemId+'"), $("#city_'+elemId+'"), $("#state_'+elemId+'"), $("#zip_'+elemId+'")], '+elemId+')\' class="form-control" placeholder="street2" /> ')
          .append('<input type="text" id="city_'+elemId+'" onchange=\'setAddress([$("#street1_'+elemId+'"), $("#street2_'+elemId+'"), $("#city_'+elemId+'"), $("#state_'+elemId+'"), $("#zip_'+elemId+'")], '+elemId+')\' class="form-control" placeholder="city" /> ')
          .append('<input type="text" id="state_'+elemId+'"onchange=\'setAddress([$("#street1_'+elemId+'"), $("#street2_'+elemId+'"), $("#city_'+elemId+'"), $("#state_'+elemId+'"), $("#zip_'+elemId+'")], '+elemId+')\' class="form-control" placeholder="state" /> ')
          .append('<input type="text" id="zip_'+elemId+'" onchange=\'setAddress([$("#street1_'+elemId+'"), $("#street2_'+elemId+'"), $("#city_'+elemId+'"), $("#state_'+elemId+'"), $("#zip_'+elemId+'")], '+elemId+')\' class="form-control" placeholder="zip" /> ')
          ; 
      initAddress( $('#address'+elemId), elemId );
      
    } else {
      // can use address as normal, so do nothing
      $('#address').show();
    }


  }
  



</script>




  
  {!! Form::close() !!}
  
  @include('communications::errors.list')

@endsection