@extends(config('communications.layout_template', 'app'))

@section('content')
  <style>
   table tbody tr:hover td {
    cursor: pointer;   
    background-color: #DDD;
  }
  </style>
  <h1>
    Communication Channels
    @if( isset($recipient) )
      for {{ $recipient->fullName() }}
      <span class="pull-right">
        <a class="btn btn-default btn-xs" href="{{ route( $recipient->recipientBaseRoute().'.channels.create', [$recipient->id]) }}">Add Channel</a>
        <a class="btn btn-default btn-xs pull-right" href="{{ route( $recipient->recipientBaseRoute().'.channels.createMultiple',[$recipient->id]) }}">Add Channels</a>
      </span>
    @else
      <span class="pull-right">
       <a class="btn btn-default btn-xs" href="{{ route('channels.create', null) }}">Add Channel</a>
       <a class="btn btn-default btn-xs" href="{{ route( 'channels.createMultiple', null) }}">Add Channels</a>
      </span>
    @endif
  </h1>

  
  <table class="table">
    <tr >
      <th>Recipient</th>
      <th>Channel Name</th>
      <th>Type</th>
      <th>Primary</th>
      <th>Address</th>
    </tr>
    @foreach($channels as $channel)

      <tr class="clickable-row" data-href='{{ route($channel->recipient->recipientBaseRoute().'.channels.show', [ $channel->recipient->id, $channel->id], [ $channel->recipient->id, $channel->id]) }}'>
        <td>
          <a href="{{ action($channel->recipient->recipientControllerShow(), ['id'=>$channel->recipient_id]) }}">{{ $channel->recipient->fullName() }}</a>
        </td>
        <td>
          <a href="{{ route('channels.show', [$channel->id]) }}">{{ $channel->name }}</a>
        <td>
          {{ $channel->channelType->name }}
        </td>
        <td>
          {{ ($channel->is_primary) ? 'Yes' : 'No' }}
        </td>
        <td>
          <a href="{{ route('channels.show', [$channel->id]) }}">{{ $channel->formattedAddress }}</a>
        </a>
      </tr>
    @endforeach
  </table>
  @if( isset($recipient) )
     <a href={{ action($recipient->recipientControllerShow(), [$recipient->id])}} class="btn btn-default pull-right">Back to Recipient</a>
  @endif
@endsection

@push('scripts')
  <script>
    $(document).ready(function($) {
        $(".clickable-row").click(function() {
            window.document.location = $(this).data("href");
        });
    });

  </script>
@endpush