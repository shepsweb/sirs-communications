@extends(config('communications.layout_template', 'app'))

@section('content')
  <h1>Communications
    @if( isset($recipient) )
      for {{ $recipient->fullName() }}</h1>
      <a href="{{ route($recipient->recipientBaseRoute().'.communications.create', [$recipient->id]) }}" class="btn pull-right">
        Add Communication
      </a>
    @else
  </h1>
   <a href="{{ route('communications.create') }}" class="btn pull-right">Add Communication</a>

  @endif
  
  <table class="table">
    <tr>
      <th>Sender</th>
      <th>Recipient</th>
      <th>Communication Link</th>
      <th>Reason</th>
      <th>Status</th>
 
    </tr>
    @foreach($communications as $communication)
      <tr>
        <td>Me</td>
        <td>
          <a href="{{ action($communication->channel->recipient->recipientControllerShow(), [$communication->channel->recipient_id]) }}">
            {{ $communication->channel->recipient->fullName() }}
          </a>
        </td>
        <td>
          <a href="{{ action('CommunicationController@show', [$communication->channel->recipient_id,$communication->id]) }}">
            {{ $communication->channel->name }}
          </a>
        </td>
        <td>{{ $communication->reason->name }}</td>
        <td>{{ $communication->status->name }}</td>
      </tr>
    @endforeach
  </table>

  @if( isset($recipient) )
     <a href="{{ action($recipient->recipientControllerShow(), [$recipient->id]) }}" class="btn btn-default pull-right">Back to Recipient</a>
  @endif
@endsection