<?php namespace Sirs\Communications\Interfaces;

interface CommunicationSender
{
    public function fullName();

    /*
     * gets communicaitons relations
     * @returns \Illuminate\Database\Eloquent\Builder
     */
    public function communications();
}
