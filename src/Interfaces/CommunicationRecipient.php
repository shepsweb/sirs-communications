<?php namespace Sirs\Communications\Interfaces;

interface CommunicationRecipient
{
    public function fullName();

    /*
     * gets channel relations
     * @returns \Illuminate\Database\Eloquent\Builder
     */
    public function channels();

    /*
     * gets communicaitons relations
     * @returns \Illuminate\Database\Eloquent\Builder
     */
    public function communications();
}
